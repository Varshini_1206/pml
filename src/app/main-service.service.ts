import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MainService {

  constructor(private http:HttpClient) { }

  getRequest(url:string,params?:any){
    return this.http.get(url, params)
  }

  getJSON(url:string){
    return this.http.get(url)
  }
}
