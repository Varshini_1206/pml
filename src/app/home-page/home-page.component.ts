import { Component, OnInit } from '@angular/core';
import {MainService} from './../main-service.service';
@Component({
  selector: 'pml-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  autoPagination: any;
  movieHeaderData:any[] = [];
  movieBodyData: any;
  constructor(private mainService:MainService) { }

  ngOnInit() {
    this.autoPagination = {isOn:true, rowsPerPage:5} //to config table for pagination
    this.setMovieTableHeader();
    let url = 'http://www.omdbapi.com/?t=Game%20of%20Thrones&Season=1&apiKey=73be4e59'; //OMDB API
    this.mainService.getRequest(url).subscribe(data=>{
      if(data!==undefined && data!==null){
        if(data['Episodes'] !== undefined) {
          this.movieBodyData = data['Episodes']
        }else {
          this.renderFromJSON()
        }
      } else {
          this.renderFromJSON()
      }
    })
    document.querySelector('body').setAttribute('style',"background: linear-gradient(0deg, rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url('./../../assets/images/cinema2.jpg') center no-repeat fixed;background-size:cover;")
  }

  renderFromJSON(){
    this.mainService.getJSON('../../assets/data/GOTEpisodesList.json').subscribe(data=>{
      if(data!==undefined && data!==null){
        if(data['Episodes'] !== undefined) {
          this.movieBodyData = data['Episodes']
        }
      } 
    })
  }

  setMovieTableHeader(){
    this.movieHeaderData = [
      {
        "value" : "Title",
        "id":"title"
      },
      {
        "value" : "Released",
        "id":"released"
      },
      {
        "value" : "Episode",
        "id":"episode"
      },
      {
        "value" : "imdbRating",
        "id":"imdbRating"
      },
      {
        "value" : "imdbID",
        "id":"imdbId"
      }
  ]
  }
}
