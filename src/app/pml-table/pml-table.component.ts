import { Component, Input, OnInit } from '@angular/core';
import * as cloneDeep from 'lodash/cloneDeep';
@Component({
  selector: 'pml-table',
  templateUrl: './pml-table.component.html',
  styleUrls: ['./pml-table.component.css']
})
export class PmlTableComponent implements OnInit {

  @Input() tableHeaderData: String[];
  @Input() tableBodyValue: any[] = [];
  @Input() autoPaginationConfiguration: any;
  @Input() filterData: any;

  tableBodyData: any[] = []
  firstPage: any[] = []
  totalRowsPerPage: number;
  totalRowsFromResponse: any;
  paginationEnabled: Boolean = false;
  totalPages: number;
  currentPage: number;
  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(valueChange) {
    if(valueChange.tableBodyValue) {
      if(this.tableBodyValue !== undefined && this.tableBodyValue.length > 0) {
        if(this.autoPaginationConfiguration !== undefined && this.autoPaginationConfiguration.isOn !== undefined && this.autoPaginationConfiguration.isOn && this.autoPaginationConfiguration.rowsPerPage !== undefined) {
          if(this.tableBodyValue.length > this.autoPaginationConfiguration.rowsPerPage) { 
            this.checkPagination();
            this.tableBodyData = this.firstPage
          } else {
            this.autoPaginationConfiguration.isOn = false;
            this.checkPagination();
            this.tableBodyData = this.tableBodyValue
          }
        } else {
          this.tableBodyData = this.tableBodyValue
        }
      }
    }
  }

  checkPagination(){
    if(this.autoPaginationConfiguration !== undefined && this.autoPaginationConfiguration.isOn !== undefined && this.autoPaginationConfiguration.isOn && this.autoPaginationConfiguration.rowsPerPage !== undefined) {
      if(this.autoPaginationConfiguration.rowsPerPage == null || this.autoPaginationConfiguration.rowsPerPage == undefined) {
        this.totalRowsPerPage = 15
      } else {
        this.totalRowsPerPage = this.autoPaginationConfiguration.rowsPerPage
      }

      this.totalRowsFromResponse = this.tableBodyValue;
      let totalPages = this.totalRowsFromResponse.length/this.totalRowsPerPage;

      if(totalPages > 1) {
        this.setPageConfig(true, totalPages, 1)
      }

      this.firstPage = [];
      for(let i = 0; i<this.totalRowsPerPage;i++){
        this.firstPage.push(this.tableBodyValue[i])
      }
      this.tableBodyData = this.firstPage;
    } else {
      this.tableBodyData = this.tableBodyValue
    }

  }

  setPageConfig(enabled, totalPages, currentPage) {
    if(enabled){
      this.totalPages = totalPages;
      if(this.totalPages > 1){
        this.paginationEnabled = true;
        this.currentPage = currentPage;
      } else {
        this.paginationEnabled = false;
      }
    } else {
      this.paginationEnabled = false;
    }
  }

  getNewPage(page) {
    if(!this.autoPaginationConfiguration.isOn) {
    } else {
      let firstPage: any = [];

      if(page == 1){
        for(let i=0;i<this.totalRowsPerPage;i++){
          firstPage.push(this.tableBodyValue[i])
        }
      } else {
        if(page * this.totalRowsPerPage < this.tableBodyValue.length){
          for(let i = (page - 1) * this.totalRowsPerPage; i < page * this.totalRowsPerPage; i++){
            firstPage.push(this.tableBodyValue[i])
          }
        } else {
          for(let i = (page - 1)* this.totalRowsPerPage; i < this.tableBodyValue.length;i++){
            firstPage.push(this.tableBodyValue[i])
          }
        }
        this.tableBodyData = cloneDeep(firstPage)
      }
      this.tableBodyData = firstPage
    }
  }

  ngAfterViewInit(){
    for(let i=0;i<this.tableHeaderData.length;i++){
      for(let j=0;j<this.tableBodyData.length;j++){
        document.querySelector('tbody').rows[j].children[i].setAttribute('data-label',this.tableHeaderData[i]['value'])
      }
    }
  }

}
