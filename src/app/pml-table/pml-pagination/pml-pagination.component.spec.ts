import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PmlPaginationComponent } from './pml-pagination.component';


describe('PmlPaginationComponent', () => {
  let component: PmlPaginationComponent;
  let fixture: ComponentFixture<PmlPaginationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmlPaginationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmlPaginationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
