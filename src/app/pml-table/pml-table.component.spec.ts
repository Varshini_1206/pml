import { async, ComponentFixture, TestBed } from '@angular/core/testing';



describe('PmlTableComponent', () => {
  let component: PmlTableComponent;
  let fixture: ComponentFixture<PmlTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PmlTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PmlTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
