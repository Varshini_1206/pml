import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'pml-pagination',
  templateUrl: './pml-pagination.component.html',
  styleUrls: ['./pml-pagination.component.css']
})
export class PmlPaginationComponent implements OnInit {

  @Input() totalNoOfPages: number;
  @Input() currentPage: number;
  @Input() totalRecordsRendered: any;
  @Output() currentPageChange = new EventEmitter();

  paginationArray = [];
  continueMarker = "...";
  constructor() { }

  ngOnInit() {
    this.arrangePagination();
    console.log('currentPAge', this.currentPage)
  }

  ngOnChanges(changes:SimpleChanges) {
    if(changes['totalNoOfPages']){
      if(this.totalNoOfPages !== null || this.totalNoOfPages !== undefined){
        this.arrangePagination()
      }
    }
  }

  arrangePagination(){
    this.paginationArray = [];
    for(let i = 0; i < this.totalNoOfPages; i++) {
      this.paginationArray.push(i + 1)
    }

    if(this.totalNoOfPages <= 10) {}
    else if(this.totalNoOfPages > 10 && this.currentPage<=10){
      let lastValue = this.paginationArray[this.paginationArray.length - 1]
      if(this.currentPage == 10 && this.totalNoOfPages == 11){}
      else if(this.currentPage == 10 && this.totalNoOfPages > 11) {
        this.paginationArray = (this.paginationArray.slice(0,10));
        this.paginationArray.push(this.continueMarker, lastValue)
      }
      else {
        this.paginationArray = (this.paginationArray.slice(0,9));
        this.paginationArray.push(this.continueMarker, lastValue)
      }
    }
    else if(this.totalNoOfPages > 10 && ((this.totalNoOfPages - this.currentPage)<=9)){
      let firstValue = this.paginationArray[0];
      this.paginationArray = (this.paginationArray.slice(this.totalNoOfPages - 10, this.paginationArray.length));
      this.paginationArray.unshift(firstValue, this.continueMarker)
    }
    else if(this.totalNoOfPages > 20 && !(this.currentPage <= 10) && !((this.totalNoOfPages - this.currentPage)<=9)) {
      let firstValue = this.paginationArray[0];
      let lastValue = this.paginationArray[this.paginationArray.length - 1];
      let showMe = [];
      for(let i = this.currentPage - 3; i <= this.currentPage + 3; i++){
        showMe.push(i)
      }
      this.paginationArray = showMe;
      this.paginationArray.unshift(firstValue,this.continueMarker);
      this.paginationArray.push(this.continueMarker,lastValue)
    }
  }

  clickedPage(page) {
    if(page !== "..." && page!==this.currentPage){
      this.currentPage = page;
      this.currentPageChange.emit(page)
      this.arrangePagination()
    }
  }

  getPrevArrowClasses(){
    let previousArrow = "prev-arrow";
    return this.currentPage < 2 ? previousArrow + "-disabled" : previousArrow
  }

  getNextArrowClases(){
    let nextArrow = "next-arrow";
    return (this.currentPage == this.totalNoOfPages) ? nextArrow + "-disabled" : nextArrow
  }

  previousPage(){
    if(this.currentPage != 1){
      this.currentPage-= 1;
      this.currentPageChange.emit(this.currentPage);
      this.arrangePagination()
    } else {
      console.log("**Reached Home Page**")
    }
  }

  nextPage() {
    if(this.currentPage < this.totalNoOfPages) {
      this.currentPage+= 1;
      this.currentPageChange.emit(this.currentPage);
      this.arrangePagination()
    } else {
      console.log("**Reached Last Page**")
    }
  }

  findCurrentPage(page){
    if(page!=='...'){
      return this.currentPage == page ? "active" : ""
    }
  }
}
